// Type Library
//
// This library converts data from one type
// to another


// define shorthand datatypes
typedef signed char sbyte;
typedef unsigned int uint;
typedef unsigned long ulong;
typedef char* string;
#ifndef null
#define null ((void*)0)
#endif

// union for data conversion
typedef union _ConvData
{
  char    Char[8];
  byte    Byte[8];
  int     Int[4];
  long    Long[2];
  float   Float[2];
  double  Double;
}ConvData;

// macro for converting hex char to bin
#define HEX_TO_BIN(ch)    (((ch) <= '9')? (ch)-'0' : (ch)-'7')
#define BIN_TO_HEX(bn)    (((bn) <= 9)? (bn)+'0' : (bn)+'7' )


// type conversion class
class _Type
{
  public:
  ConvData Data;
  
  public:
  // converts bytes to int
  int ToInt(void *bytes)
  {
    return *((int*)bytes);
  }
  
  
  // converts 2 chars to int
  int ToInt(char char0, char char1)
  {
    Data.Char[0] = char0;
    Data.Char[1] = char1;
    return Data.Int[0];
  }
  
  
  // converts bytes to long
  long ToLong(void *bytes)
  {
    return *((long*)bytes);
  }


  // converts 2 ints to long
  long ToLong(int int0, int int1)
  {
    Data.Int[0] = int0;
    Data.Int[1] = int1;
    return Data.Long[0];
  }

  // converts 4 chars to long
  long ToLong(char char0, char char1, char char2, char char3)
  {
    Data.Char[0] = char0;
    Data.Char[1] = char1;
    Data.Char[2] = char2;
    Data.Char[3] = char3;
    return Data.Long[0];
  }


  // converts bytes to float
  float ToFloat(void *bytes)
  {
    return *((float*)bytes);
  }


  // converts 2 ints to float
  float ToFloat(int int0, int int1)
  {
    Data.Int[0] = int0;
    Data.Int[1] = int1;
    return Data.Float[0];
  }
  
  
  // converts 4 chars to float
  float ToFloat(char char0, char char1, char char2, char char3)
  {
    Data.Char[0] = char0;
    Data.Char[1] = char1;
    Data.Char[2] = char2;
    Data.Char[3] = char3;
    return Data.Float[0];
  }


  // converts bytes to float
  double ToDouble(void *bytes)
  {
    return *((double*)bytes);
  }


  // converts 2 longs to double
  double ToDouble(long long0, long long1)
  {
    Data.Long[0] = long0;
    Data.Long[1] = long1;
    return Data.Double;
  }
  
  
  // converts 4 ints to double
  double ToDouble(int int0, int int1, int int2, int int3)
  {
    Data.Int[0] = int0;
    Data.Int[1] = int1;
    Data.Int[2] = int2;
    Data.Int[3] = int3;
    return Data.Double;
  }
 
  
  // converts 8 chars to double
  double ToDouble(char char0, char char1, char char2, char char3, char char4, char char5, char char6, char char7)
  {
    Data.Char[0] = char0;
    Data.Char[1] = char1;
    Data.Char[2] = char2;
    Data.Char[3] = char3;
    Data.Char[4] = char4;
    Data.Char[5] = char5;
    Data.Char[6] = char6;
    Data.Char[7] = char7;
    return Data.Double;
  }


  // converts hex (in big-endian form) to binary (in little-endian form)
  void HexToBin(void* bin, int bin_sz, char *hex)
  {
    char* cbin = (char*)bin;
    byte hlen = strlen(hex);
    hex += hlen - 1; bin_sz <<= 1;
    for(byte i=0; i<bin_sz; i++, cbin++)
    {
      *cbin = (i >= hlen)? 0 : HEX_TO_BIN(*hex); hex--; i++;
      *cbin |= (i >= hlen)? 0 : HEX_TO_BIN(*hex) << 4; hex--; i++;
    }
  }


  // converts binary (in little-endian form) to hex (in big-endian form)
  void BinToHex(char *hex, void* bin, int bin_sz)
  {
    char* cbin = ((char*)bin) + bin_sz - 1;
    for(byte i=0; i<bin_sz; i++, cbin--)
    {
      if((*cbin & 0xF0) != 0) {*hex = BIN_TO_HEX(*cbin >> 4); hex++;}
      if((*cbin & 0x0F) != 0) {*hex = BIN_TO_HEX(*cbin & 0xF); hex++;}
    }
    *hex = '\0';
  }
}Type;
