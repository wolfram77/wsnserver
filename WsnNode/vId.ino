// Arduino Identity
//
// This section should contain all identity related info.

class IdClass
{
  public:
  // Device Nomenclature
  char  Code[8];
  char  Name[32];
  char  Dsc[64];
  char  Loc[64];
  byte  Lvl;
  
  public:
  // set default id
  void SetDefault()
  {
    strcmp(Name, "Chakra");
    strcpy(Dsc,  "Arduino Mega 2560");
    strcpy(Loc,  "NUS, WS2, RM2, RT-CRNR");
    Lvl  = 5;
  }
  
  
  // fetches id info XBee 
  int FetchFromXbee()
  {
    
  }
  
  
  // generate id code randomly
  void GenCodeRandom()
  {
    for(byte i=0; i<8; i++) Code[i] = (char)random(256);
  }
  
  
  // save id to ROM
  byte Save()
  {
    Rom.Write("Id.Code", Code, 8);
    Rom.Write("Id.Name", Name, strlen(Name)+1);
    Rom.Write("Id.Dsc", Dsc, strlen(Dsc)+1);
    Rom.Write("Id.Loc", Loc, strlen(Loc)+1);
    Rom.Write("Id.Lvl", &Lvl, 1);
    if(Rom.Error) return -1;
    return 0;
  }
  
  
  // load id from ROM
  byte Load()
  {
    Rom.Read("Id.Code", Code, 8);
    Rom.Read("Id.Name", Name, 32);
    Rom.Read("Id.Dsc", Dsc, 64);
    Rom.Read("Id.Loc", Loc, 64);
    Rom.Read("Id.Lvl", &Lvl, 1);
    if(Rom.Error) return -1;
    return 0;
  }
  
  
  // initlialize id (to be executed on setup)
  void Init()
  {
    Serial.println("Loading ID...");
    if(Load() != -1) return;
    Serial.println("Loading failed.");
    GenCodeRandom();
    Save();
  }
}Id;


