// Main Code
//
// All main execution code goes here

// Read a set of data from 2 adc pins and send it to xbee
int pinV = A2;
int pinI = A3;
int pinLed = 13;
int valV[16];
int valI[16];
int ptr, ledC;
byte Buff[128];

// finds the sum of bytes in an array from start to stop
byte doSum(byte* dat, int start, int stp)
{
  int i; byte sum = 0;
  for(i=start; i<=stp; i++)
    sum += dat[i];
  return sum;
}

// runs once on setup
void start()
{
  ptr = 0;
  ledC = 0;
  pinMode(pinLed, OUTPUT);
  pinMode(pinV, INPUT);
  pinMode(pinI, INPUT);
  analogReference(DEFAULT);
  randomSeed(analogRead(0));
  Serial.begin(9600);
}

// runs always on loop (after start)
void always()
{
  int i, p;
  
  if(ptr < 16)
  {
    valV[ptr] = analogRead(pinV);
    valI[ptr] = analogRead(pinI);
    ptr++;
    delay(1);
    return;
  }
  ledC = ledC ^ 1;
  if(ledC) digitalWrite(pinLed, HIGH);
  else digitalWrite(pinLed, LOW);
  Buff[0] = 0x7E;  // start del
  Buff[1] = 0x00;  // len msb
  Buff[2] = 72;    // len lsb
  Buff[3] = 0x01;  // api id
  Buff[4] = 0x01;  // frame id
  Buff[5] = 0x00;  // dest msb
  Buff[6] = 0x00;  // dest lsb
  Buff[7] = 0x00;  // option
  Buff[8] = 16;    // samples
  Buff[9] = 0x00;  // channel msb
  Buff[10] = 0x30; // channel lsb
  p = 11;
  for(i=0; i<16; i++)
  {
    Buff[p] = (byte)((valV[i] >> 8) & 0xFF);
    Buff[p+1] = (byte)(valV[i] & 0xFF);
    Buff[p+2] = (byte)((valI[i] >> 8) & 0xFF);
    Buff[p+3] = (byte)(valI[i] & 0xFF);
    p += 4;
  }
  Buff[p] = (0xFF - doSum(Buff, 3, p-1)) & 0xFF;
  Serial.write(Buff, p+1);
  ptr = 0;
  delay(1);
}


