// ROM Library
//
// This library writes/fetches the req. field 
// to/from EEPROM
//
// Fields are stored in EEPROM like this:
// <int eeprom_data_used> (excluding this int)
// <char* field_name><int field_length><byte field_data[]>
// <char* field_name><int field_length><byte field_data[]>...
//


#include <EEPROM.h>

class RomClass
{
  public:
  // size of EEPROM memory
  const static int Size = 2048;
  
  public:
  // global status
  boolean Error;
  
  public:
  // fetches the address of a field
  int FetchFieldAddr(char* field)
  {
    int ptr, si, len, sz;
    
    // fetch the amount of stored data
    si = 0;
    ptr = 2;
    sz = 2 + Type.ToInt(EEPROM.read(0), EEPROM.read(1));
    // browse through all stored data
    while(ptr < sz)
    {
      // match the fields
      if(field[si] == EEPROM.read(ptr))
      {
        // exact match found! tell the address (to length)
        // only if len != 0 (== 0 implies invalid field)
        if(field[si] == '\0')
        {
          len = Type.ToInt(EEPROM.read(ptr+1), EEPROM.read(ptr+2));
          if(len == 0) {ptr += 3; continue;}
          return ptr + 1;
        }
        si++; ptr++;
        continue;
      }
      // jump to next field
      si = 0;
      while(EEPROM.read(ptr) != '\0' && ptr < sz) ptr++;
      len = Type.ToInt(EEPROM.read(ptr+1), EEPROM.read(ptr+2));
      ptr += 3 + len;
    }
    // did not find? report that error!
    Error = true;
    return -1;
  }


  // reads bytes from field
  void* Read(char* field, void* str_buff, int buff_sz)
  {
    char* dst = (char*) str_buff;
    int i, len, ptr = FetchFieldAddr(field);
    if(ptr == -1) return (void*)0;
    len = Type.ToInt(EEPROM.read(ptr), EEPROM.read(ptr+1));
    len = min(len, buff_sz);
    ptr += 2;
    for(i=0; i<len; i++, ptr++) dst[i] = EEPROM.read(ptr);
    return str_buff;
  }
  

  // gets the free space available
  int GetFreeSpace()
  {
    int used = 2 + Type.ToInt(EEPROM.read(0), EEPROM.read(1));
    return Size - used;
  }
  
  
  // clear all data stored
  void Clear()
  {
    EEPROM.write(0, 0);
    EEPROM.write(1, 0);
  }
  
  
  // writes bytes to field
  int Write(char* field, void* val, int len)
  {
    char *cval = (char*)val;
    int i, pptr, ptr, sz = strlen(field) + 3 + len;
    ptr = 2 + Type.ToInt(EEPROM.read(0), EEPROM.read(1));
    if(sz > Size - ptr) {Error = true; return -1;}
    pptr = FetchFieldAddr(field);
    if(pptr != -1)
    {
      EEPROM.write(pptr, 0);
      EEPROM.write(pptr+1, 0);
    }
    sz = strlen(field) + 1;
    for(i=0; i<sz; i++, ptr++) EEPROM.write(ptr, field[i]);
    Type.Data.Int[0] = len;
    EEPROM.write(ptr, Type.Data.Char[0]);
    EEPROM.write(ptr+1, Type.Data.Char[1]);
    ptr += 2;
    for(i=0; i<len; i++, ptr++) EEPROM.write(ptr, cval[i]);
    Type.Data.Int[0] = ptr - 2;
    EEPROM.write(0, Type.Data.Char[0]);
    EEPROM.write(1, Type.Data.Char[1]);
    return 0;
  }
}Rom;


