// 
// Packet dealing module
// 

// get ckecksum value for a packet
short PktSum(byte[] dat, int start, int stop)
{ short sum = 0;
  for(int i=start; i<stop; i+=2)
  { sum += GetShort(dat, i); }
  return sum; }

// main packet class
class Pkt
{ // constants
  static final int StartId = 0xE1;
  
  // properties
  short Length, ChkSum;
  byte InfTyp;
  long NodeId;
  String NodeNm, NodePlc;
  String NodeTyp, NodeTag;
  double[] NodeLoc;
  long SnsrId;
  short SnsrTyp;
  double[] SnsrLoc;
  long CapTm, CapInt;   // micros since 2000
  short CapSz;
  int CapDatPtr;
  
  // null init
  void Init()
  { Length = 0; ChkSum = 0;
    InfTyp = 0; NodeId = 0;
    NodeNm = null; NodePlc = null;
    NodeTyp = null; NodeTag = null;
    NodeLoc = null;
    SnsrId = 0; SnsrTyp = 0;
    SnsrLoc = null;
    CapTm = 0; CapInt = 0;
    CapDatPtr = 0; }
  
  // null init
  Pkt()
  { Init(); }
  
  // init from pkt packet
  void Init(byte[] pkt)
  { int ptr, len;
    short fld, i;
    byte[] ba;
    // initialize
    Init();
    // validate packet
    if(pkt == null || pkt.length == 0 || pkt[0] != StartId) return;
    len = pkt.length; Length = GetShort(pkt, 1);
    if(Length + 3 != len) { Length = 0; return; }
    ChkSum = GetShort(pkt, 3);
    if(ChkSum != PktSum(pkt, 5, len)) { Length = 0; return; }
    // get packet info
    InfTyp = pkt[5]; ptr = 6;
    while(ptr < len)
    { // get field type
      fld = pkt[ptr];
      if(fld < 0) { GetShort(pkt, ptr); ptr++; } ptr++;
      switch(fld)
      { // node info fields
        // node id
        case 2:
        NodeId = GetLong(pkt, ptr);
        ptr += 8; break;
        // node name
        case 4:
        NodeNm = GetString(pkt, ptr);
        ptr += NodeNm.length() + 2; break;
        // node place
        case 6:
        NodePlc = GetString(pkt, ptr);
        ptr += NodePlc.length() + 2; break;
        // node type
        case 8:
        NodeTyp = GetString(pkt, ptr);
        ptr += NodeTyp.length() + 2; break;
        // node tags
        case 10:
        NodeTag = GetString(pkt, ptr);
        ptr += NodeTag.length() + 2; break;
        // node location
        case 12:
        NodeLoc = new double[3];
        NodeLoc[0] = GetDouble(pkt, ptr); NodeLoc[1] = GetDouble(pkt, ptr + 8);
        NodeLoc[2] = GetDouble(pkt, ptr + 16); ptr += 24; break;
        // sensor info fields
        // sensor id
        case 3:
        SnsrId = GetLong(pkt, ptr);
        ptr += 8; break;
        // sensor type
        case 5:
        SnsrTyp = GetShort(pkt, ptr);
        ptr += 2; break;
        // sensor position
        case 7:
        SnsrLoc = new double[3];
        SnsrLoc[0] = GetDouble(pkt, ptr); SnsrLoc[1] = GetDouble(pkt, ptr + 8);
        SnsrLoc[2] = GetDouble(pkt, ptr + 16); ptr += 24; break;
        // capture time
        case 9:
        CapTm = GetLong(pkt, ptr);
        ptr += 8; break;
        // capture interval
        case 11:
        CapInt = GetLong(pkt, ptr);
        ptr += 8; break;
        // capture size
        case 13:
        CapSz = GetShort(pkt, ptr);
        ptr += 2; break;
        // capture data
        case 15:
        CapDatPtr = ptr;
        ptr += CapSz; break; } } }
  
  // init from pkt packet
  Pkt(byte[] pkt)
  { Init(pkt); }
}

