// 
// File Queue dealing module
// (byte-sized)
// 

import java.io.*;

interface FilqEvent
{ void Handler(Filq queue, int bytes_onremove); }

class FilqException extends Exception
{ FilqException()
  { super(); }
  FilqException(String msg)
  { super(msg); }
  FilqException(String msg, Throwable cs)
  { super(msg, cs); }
  FilqException(Throwable cs)
  { super(cs); } }

class Filq
{ // properties
  int Rear, Front;
  int Count, Size;
  RandomAccessFile File;
  FilqEvent AutoDequeueHandler;
  static final String ErrUndrFlw = "Filq: Underflow: The queue has fewer bytes than requested";
  static final String ErrOvrFlw =  "Filq: Overflow: The requested bytes do not fit into the queue";

  // write properties
  void WriteProp() throws Exception
  { byte[] buf = new byte[16];
    PutInt(buf, 0, Rear); PutInt(buf, 4, Front);
    PutInt(buf, 8, Count); PutInt(buf, 12, Size);
    File.seek(0); File.write(buf); }
  
  // read properties
  void ReadProp() throws Exception
  { byte[] buf = new byte[16];
    File.seek(0); File.readFully(buf);
    Rear = GetInt(buf, 0); Front = GetInt(buf, 4);
    Count = GetInt(buf, 8); Size = GetInt(buf, 12); }
  
  // init queue
  void Init(String file, int size, boolean is_new, FilqEvent autodequeue_handler) throws Exception
  { File = new RandomAccessFile(file, "rw");
    if(is_new)
    { Rear = 0; Front = 0; Count = 0; Size = size;
      AutoDequeueHandler = autodequeue_handler;
      WriteProp(); File.setLength(16 + size); }
    else ReadProp(); }
  
  // init queue
  Filq(String file, int size, boolean is_new, FilqEvent autodequeue_handler) throws Exception
  { Init(file, size, is_new, autodequeue_handler); }
  
  // clear queue
  void Clear()
  { Rear = 0; Front = 0;
    Count = 0; }
  
  // close queue
  void Close() throws Exception
  { WriteProp(); File.close(); }
  
  // get bytes from rear
  void GetFromRear(byte[] dst, int off, int len) throws Exception
  { int rd, rear = Rear;
    if(Count < len)
    { throw new FilqException(ErrUndrFlw); }
    if(rear <= Front)
    { rd = min(rear, len);
      File.seek(32 + rear - rd); File.readFully(dst, off + len - rd, rd);
      len -= rd; rear = Size; } 
    if(len > 0)
    { File.seek(32 + rear - len); File.readFully(dst, off, len); } }
  
  // get bytes from front
  void GetFromFront(byte[] dst, int off, int len) throws Exception
  { int rd, front = Front;
    if(Count < len)
    { throw new FilqException(ErrUndrFlw); }
    if(front >= Rear)
    { rd = min(Size - front, len);
      File.seek(32 + front); File.readFully(dst, off, rd);
      off += rd; len -= rd; front = 0; } 
    if(len > 0)
    { File.seek(32 + front); File.readFully(dst, off, len); } }
  
  // get and remove bytes from front
  void Dequeue(byte[] dst, int off, int len) throws Exception
  { if(dst != null) GetFromFront(dst, off, len);
    Front += len; Front = (Front < Size)? Front : Front - Size; }

  // put bytes to rear
  // note: if enough space is not available, auto dequeue
  //       event handler is called after which req. data
  //       is dequeued (use event handler to perform special
  //       functions)
  void Enqueue(byte[] src, int off, int len) throws Exception
  { int rmv, ins;
    if(len > Size)
    { throw new FilqException(ErrOvrFlw); }
    if(Count + len > Size)
    { rmv = Count + len - Size;
      if(AutoDequeueHandler != null)
      { AutoDequeueHandler.Handler(this, rmv); }
      Dequeue(null, 0, rmv); }
    ins = min(Size - Rear, len);
    File.seek(16 + Rear); File.write(src, off, ins);
    off += ins; len -= ins;
    Rear += ins; Rear = (Rear < Size)? Rear : Rear - Size;
    if(len > 0)
    { File.seek(16 + Rear); File.write(src, off, len);
      Rear += len; Rear = (Rear < Size)? Rear : Rear - Size; } } }

