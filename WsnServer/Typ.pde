// 
// Type conversion module
// 

import java.nio.*;

// gets a short from byte array
short GetShort(byte[] dat, int off)
{ return ByteBuffer.wrap(dat, off, 2).getShort(); }

// gets a char from byte array
char GetChar(byte[] dat, int off)
{ return ByteBuffer.wrap(dat, off, 2).getChar(); }

// gets an int from byte array
int GetInt(byte[] dat, int off)
{ return ByteBuffer.wrap(dat, off, 4).getInt(); }

// gets a long from byte array
long GetLong(byte[] dat, int off)
{ return ByteBuffer.wrap(dat, off, 8).getLong(); }

// gets a float from byte array
float GetFloat(byte[] dat, int off)
{ return ByteBuffer.wrap(dat, off, 4).getFloat(); }

// gets a double from byte array
double GetDouble(byte[] dat, int off)
{ return ByteBuffer.wrap(dat, off, 8).getDouble(); }

// gets a lascii string from byte array
String GetString(byte[] dat, int off)
{ return new String(dat, off+2, GetShort(dat, off)); }

// puts a short to byte array
void PutShort(byte[] dat, int off, short val)
{
  ByteBuffer b = ByteBuffer.allocate(2).putShort(val);
  b.position(0); b.get(dat, off, 2);
}

// puts a char to byte array
void PutChar(byte[] dat, int off, char val)
{
  ByteBuffer b = ByteBuffer.allocate(2).putChar(val);
  b.position(0); b.get(dat, off, 2);
}

// puts an int to byte array
void PutInt(byte[] dat, int off, int val)
{
  ByteBuffer b = ByteBuffer.allocate(4).putInt(val);
  b.position(0); b.get(dat, off, 4);
}

// puts a long to byte array
void PutLong(byte[] dat, int off, long val)
{
  ByteBuffer b = ByteBuffer.allocate(8).putLong(val);
  b.position(0); b.get(dat, off, 8);
}

// puts a float to byte array
void PutFloat(byte[] dat, int off, float val)
{
  ByteBuffer b = ByteBuffer.allocate(4).putFloat(val);
  b.position(0); b.get(dat, off, 4);
}

// puts a double to byte array
void PutDouble(byte[] dat, int off, double val)
{
  ByteBuffer b = ByteBuffer.allocate(8).putDouble(val);
  b.position(0); b.get(dat, off, 8);
}

// puts a lascii string to byte array
void PutString(byte[] dat, int off, String val)
{
  PutShort(dat, off, (short)val.length());
  arrayCopy(val.getBytes(), 0, dat, off+2, val.length());
}

