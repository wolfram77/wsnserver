/**
* Wireless Sensor Network Server with UI
*
* ...
* ...
*
*/

float ptX[], ptY[], ptVal[];

Host   wHost;
PImage Field;

void pointGen()
{
  ptX = new float[20];
  ptY = new float[20];
  ptVal = new float[20];
  for(int i=0; i<20; i++)
  {
    ptX[i] = random(width);
    ptY[i] = random(height);
    ptVal[i] = random(1); 
  }
}

void pointUpdate()
{
  for(int i=0; i<20; i++)
  {
    ptVal[i] += random(0.1) - 0.05;
    ptVal[i] = (ptVal[i] < 0)? 0 : ((ptVal[i] > 1)? 1 : ptVal[i]);
  }
}

void pointDraw()
{
  ellipseMode(CENTER);
  for(int i=0; i<20; i++)
  {
    fill(255*ptVal[i], 255*ptVal[i], 255*ptVal[i]);
    ellipse(ptX[i], ptY[i], 5, 5);
  }
}

void fieldDraw()
{
  // sinc factor
  float factor = 0.1;
  Field.loadPixels();
  // calculate value average, max
  float avgVal = 0.0f, maxVal = 0.0f;
  for(int i=0; i<20; i++)
  {
    avgVal += ptVal[i];
    maxVal = (ptVal[i] > maxVal)? ptVal[i] : maxVal;
  }
  avgVal /= 20;
  maxVal -= avgVal;
  // calculate color for every point
  for(int y=0, px = 0; y<height; y++)
  {
    for(int x=0; x<width; x++, px++)
    {
      // get the value component from every node
      float dist, valsinc, val = 0.0f;
      for(int i=0; i<20; i++)
      {
        dist = sqrt(pow(x - ptX[i], 2) + pow(y - ptY[i], 2));
        valsinc = (dist < 0.001f)? 1 : (sin(factor * dist) / dist);
        val += (ptVal[i] - avgVal) * valsinc; 
      }
      val *= 4;
      val += avgVal;
      Field.pixels[px] = color(255 * val, 255 * val, 255 * val);
    }
  }
  Field.updatePixels();
  image(Field, 0, 0);
}

void setup()
{
  WinInit();
  byte[] x = new byte[] {1, 2, 3, 4, 5, 6, 7, 8};
  PutString(x, 0, "sdsad");
  println(GetString(x, 0));
  pointGen();
  Field = createImage(width, height, RGB);
}

void draw()
{
  WinUpdate();
  pointUpdate();
  fieldDraw();
  pointDraw();
}

