// 
// Window management module
// 

import controlP5.*;


ControlP5 WinUi;
String textValue = "";
float ViewZoom = 0.0f, ViewZoomMax = 5.0f, ViewZoomMin = -5.0f;

void WinInit()
{
  size(200, 200);
  TxtInit();
  // Window components
  WinUi = new ControlP5(this);
  WinUi.addToggle("SrvrTogg").setPosition(462, 4).setSize(100, 40)
    .setCaptionLabel("WSN Server\n(Offline)").registerTooltip("Click to start/stop the server")
    .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER);
  WinUi.addTextfield("Port").setPosition(563, 4).setSize(60, 40)
    .setFont(FntUi).setFocus(true);
  WinUi.addSlider("Zoom").setPosition(217, 14).setSize(200, 20)
    .setCaptionLabel("").setRange(ViewZoomMin, ViewZoomMax).setValue(ViewZoom);
  WinUi.addButton("ZoomIn").setValue(2).setPosition(422, 14).setSize(20, 20)
    .setCaptionLabel("+").getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER);
  WinUi.addButton("ZoomOut").setValue(3).setPosition(192, 14).setSize(20, 20)
    .setCaptionLabel("-").getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER);
}

void WinUpdate()
{
  background(0);
  fill(255);
}



// window event handlers
void SrvrTogg(boolean togg)
{
  if(togg) wHost = new Host(this, WinUi.get(Textfield.class, "Port").getText());
  else wHost.Close();
  WinUi.get(Controller.class, "SrvrTogg").setCaptionLabel("WSN Server\n("+wHost.Ip+")");
  WinUi.get(Textfield.class, "Port").setText(wHost.Port+"");
}

void ZoomIn(int val)
{
  ViewZoom = min(ViewZoom + 1, ViewZoomMax);
  WinUi.get(Controller.class, "Zoom").setValue(ViewZoom);
}

void ZoomOut(int val)
{
  ViewZoom = max(ViewZoom - 1, ViewZoomMin);
  WinUi.get(Controller.class, "Zoom").setValue(ViewZoom);
}

void Zoom(float zoom)
{
  ViewZoom = zoom;
}

void controlEvent(ControlEvent e)
{
  if(e.isAssignableFrom(Textfield.class))
  {
    println("global: "+e.getName()+": "+e.getStringValue());
  }
}

