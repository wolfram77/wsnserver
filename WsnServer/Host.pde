// 
// Host module
// 

import processing.net.*;

class Host
{
Server Srvr;
String Name, Ip;
String Msg;
int Port;
boolean Error;

// initialize host with a port number
void Init(PApplet app, int port)
{
  Srvr = null;
  Ip = Server.ip();
  Port = -1;
  Error = true;
  try
  {
    Srvr = new Server(app, port);
    Port = port;
  }
  catch(Exception e) {Msg = "Host(" + Ip + ":" + port + ") initialization failed."; return;}
  Name = "Host(" + Ip + ":" + Port + ")";
  Msg = Name + " initialized.";
  Error = false;
}


void Init(PApplet app, String port)
{
  Error = true;
  try
  {
    int portno = Integer.parseInt(port);
    portno = (portno < 0 || portno > 65535)? 24 : portno;
    Init(app, portno);
  }
  catch(Exception e) {Msg = "Illegal Port number for Host."; return;}
  Error = false;
}


Host(PApplet app, String port)
{
  Init(app, port);
}


void Close()
{
  Error = true;
  try
  {
    Srvr.stop();
    Srvr = null;
    Port = -1;
    Name = "Host(" + Ip + ":" + Port + ")";
  }
  catch(Exception e) {Msg = "Failed to stop " + Name + "."; return;}
  Msg = Name + " was closed.";
  Error = false;
}
}

